/*! \file AckI2C.c
 *
 *  \brief  Generates an Acknowledge.
 *
 *
 *  \date 21-Nov-12
 *  \author JJMcD
 *
 */

#include <xc.h>
#include "i2c.h"

/*! Generates an Acknowledge.   */
/*! Clears the acknowledge data bit, then sets the acknowledge
 *  sequence enable bit which initiates the acknowledge transmission.
 *  Then waits for the acknowledge sequence enable bit to clear
 *  indicating that the acknowledge has been sent.
 *
 *  \param none
 *  \return none
 *
 */
void AckI2C(void)
{
    IdleI2C();
    I2CCONbits.ACKDT = 0;       /* Set for ACK                  */
    I2CCONbits.ACKEN = 1;       /* Initiate ACK sequence        */
    while(I2CCONbits.ACKEN);    /* wait for ACK to complete     */
}
