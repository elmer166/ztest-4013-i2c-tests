/*! \file IdleI2C.c
 *
 *  \brief  Waits for bus to become Idle
 *
 *
 *  \date 21-Nov-12
 *  \author JJMcD
 *
 */

#include <xc.h>
#include "i2c.h"

/*! Waits for bus to become Idle.   */
/*! Watches the transmit status bit and exits when the
 *  bit is cleared.
 *
 *  Technically this doesn't mean the bus is clear, but the part cannot
 *  transmit while a receive is in progress, so essentially, ensuring the
 *  transmit buffer is full is adequate.
 *
 *  \param none
 *  \return none
 */
void IdleI2C(void)
{
    while (I2CSTATbits.TRSTAT);     /* Wait for bus Idle        */
}

