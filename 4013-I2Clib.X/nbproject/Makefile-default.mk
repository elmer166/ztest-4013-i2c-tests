#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=a
DEBUGGABLE_SUFFIX=a
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/4013-I2Clib.X.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=a
DEBUGGABLE_SUFFIX=a
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/4013-I2Clib.X.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/AckI2C.o ${OBJECTDIR}/ACKstatusI2C.o ${OBJECTDIR}/getI2C.o ${OBJECTDIR}/IdleI2C.o ${OBJECTDIR}/InitI2C.o ${OBJECTDIR}/MB85RC16VreadByte.o ${OBJECTDIR}/MB85RC16VwriteByte.o ${OBJECTDIR}/MCP23008readRegister.o ${OBJECTDIR}/MCP23008writeRegister.o ${OBJECTDIR}/MCP4726write.o ${OBJECTDIR}/NotAckI2C.o ${OBJECTDIR}/RestartI2C.o ${OBJECTDIR}/StartI2C.o ${OBJECTDIR}/StopI2C.o ${OBJECTDIR}/WriteI2C.o
POSSIBLE_DEPFILES=${OBJECTDIR}/AckI2C.o.d ${OBJECTDIR}/ACKstatusI2C.o.d ${OBJECTDIR}/getI2C.o.d ${OBJECTDIR}/IdleI2C.o.d ${OBJECTDIR}/InitI2C.o.d ${OBJECTDIR}/MB85RC16VreadByte.o.d ${OBJECTDIR}/MB85RC16VwriteByte.o.d ${OBJECTDIR}/MCP23008readRegister.o.d ${OBJECTDIR}/MCP23008writeRegister.o.d ${OBJECTDIR}/MCP4726write.o.d ${OBJECTDIR}/NotAckI2C.o.d ${OBJECTDIR}/RestartI2C.o.d ${OBJECTDIR}/StartI2C.o.d ${OBJECTDIR}/StopI2C.o.d ${OBJECTDIR}/WriteI2C.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/AckI2C.o ${OBJECTDIR}/ACKstatusI2C.o ${OBJECTDIR}/getI2C.o ${OBJECTDIR}/IdleI2C.o ${OBJECTDIR}/InitI2C.o ${OBJECTDIR}/MB85RC16VreadByte.o ${OBJECTDIR}/MB85RC16VwriteByte.o ${OBJECTDIR}/MCP23008readRegister.o ${OBJECTDIR}/MCP23008writeRegister.o ${OBJECTDIR}/MCP4726write.o ${OBJECTDIR}/NotAckI2C.o ${OBJECTDIR}/RestartI2C.o ${OBJECTDIR}/StartI2C.o ${OBJECTDIR}/StopI2C.o ${OBJECTDIR}/WriteI2C.o


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/4013-I2Clib.X.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=30F4013
MP_LINKER_FILE_OPTION=,--script=p30F4013.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/AckI2C.o: AckI2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/AckI2C.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  AckI2C.c  -o ${OBJECTDIR}/AckI2C.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/AckI2C.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/AckI2C.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/ACKstatusI2C.o: ACKstatusI2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/ACKstatusI2C.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ACKstatusI2C.c  -o ${OBJECTDIR}/ACKstatusI2C.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/ACKstatusI2C.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/ACKstatusI2C.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/getI2C.o: getI2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/getI2C.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  getI2C.c  -o ${OBJECTDIR}/getI2C.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/getI2C.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/getI2C.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/IdleI2C.o: IdleI2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/IdleI2C.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  IdleI2C.c  -o ${OBJECTDIR}/IdleI2C.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/IdleI2C.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/IdleI2C.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/InitI2C.o: InitI2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/InitI2C.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  InitI2C.c  -o ${OBJECTDIR}/InitI2C.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/InitI2C.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/InitI2C.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/MB85RC16VreadByte.o: MB85RC16VreadByte.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/MB85RC16VreadByte.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  MB85RC16VreadByte.c  -o ${OBJECTDIR}/MB85RC16VreadByte.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/MB85RC16VreadByte.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/MB85RC16VreadByte.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/MB85RC16VwriteByte.o: MB85RC16VwriteByte.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/MB85RC16VwriteByte.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  MB85RC16VwriteByte.c  -o ${OBJECTDIR}/MB85RC16VwriteByte.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/MB85RC16VwriteByte.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/MB85RC16VwriteByte.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/MCP23008readRegister.o: MCP23008readRegister.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/MCP23008readRegister.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  MCP23008readRegister.c  -o ${OBJECTDIR}/MCP23008readRegister.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/MCP23008readRegister.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/MCP23008readRegister.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/MCP23008writeRegister.o: MCP23008writeRegister.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/MCP23008writeRegister.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  MCP23008writeRegister.c  -o ${OBJECTDIR}/MCP23008writeRegister.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/MCP23008writeRegister.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/MCP23008writeRegister.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/MCP4726write.o: MCP4726write.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/MCP4726write.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  MCP4726write.c  -o ${OBJECTDIR}/MCP4726write.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/MCP4726write.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/MCP4726write.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/NotAckI2C.o: NotAckI2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/NotAckI2C.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  NotAckI2C.c  -o ${OBJECTDIR}/NotAckI2C.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/NotAckI2C.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/NotAckI2C.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/RestartI2C.o: RestartI2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/RestartI2C.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  RestartI2C.c  -o ${OBJECTDIR}/RestartI2C.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/RestartI2C.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/RestartI2C.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/StartI2C.o: StartI2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/StartI2C.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  StartI2C.c  -o ${OBJECTDIR}/StartI2C.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/StartI2C.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/StartI2C.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/StopI2C.o: StopI2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/StopI2C.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  StopI2C.c  -o ${OBJECTDIR}/StopI2C.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/StopI2C.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/StopI2C.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/WriteI2C.o: WriteI2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/WriteI2C.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  WriteI2C.c  -o ${OBJECTDIR}/WriteI2C.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/WriteI2C.o.d"        -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/WriteI2C.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/AckI2C.o: AckI2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/AckI2C.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  AckI2C.c  -o ${OBJECTDIR}/AckI2C.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/AckI2C.o.d"        -g -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/AckI2C.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/ACKstatusI2C.o: ACKstatusI2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/ACKstatusI2C.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ACKstatusI2C.c  -o ${OBJECTDIR}/ACKstatusI2C.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/ACKstatusI2C.o.d"        -g -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/ACKstatusI2C.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/getI2C.o: getI2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/getI2C.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  getI2C.c  -o ${OBJECTDIR}/getI2C.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/getI2C.o.d"        -g -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/getI2C.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/IdleI2C.o: IdleI2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/IdleI2C.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  IdleI2C.c  -o ${OBJECTDIR}/IdleI2C.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/IdleI2C.o.d"        -g -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/IdleI2C.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/InitI2C.o: InitI2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/InitI2C.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  InitI2C.c  -o ${OBJECTDIR}/InitI2C.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/InitI2C.o.d"        -g -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/InitI2C.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/MB85RC16VreadByte.o: MB85RC16VreadByte.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/MB85RC16VreadByte.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  MB85RC16VreadByte.c  -o ${OBJECTDIR}/MB85RC16VreadByte.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/MB85RC16VreadByte.o.d"        -g -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/MB85RC16VreadByte.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/MB85RC16VwriteByte.o: MB85RC16VwriteByte.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/MB85RC16VwriteByte.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  MB85RC16VwriteByte.c  -o ${OBJECTDIR}/MB85RC16VwriteByte.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/MB85RC16VwriteByte.o.d"        -g -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/MB85RC16VwriteByte.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/MCP23008readRegister.o: MCP23008readRegister.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/MCP23008readRegister.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  MCP23008readRegister.c  -o ${OBJECTDIR}/MCP23008readRegister.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/MCP23008readRegister.o.d"        -g -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/MCP23008readRegister.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/MCP23008writeRegister.o: MCP23008writeRegister.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/MCP23008writeRegister.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  MCP23008writeRegister.c  -o ${OBJECTDIR}/MCP23008writeRegister.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/MCP23008writeRegister.o.d"        -g -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/MCP23008writeRegister.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/MCP4726write.o: MCP4726write.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/MCP4726write.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  MCP4726write.c  -o ${OBJECTDIR}/MCP4726write.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/MCP4726write.o.d"        -g -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/MCP4726write.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/NotAckI2C.o: NotAckI2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/NotAckI2C.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  NotAckI2C.c  -o ${OBJECTDIR}/NotAckI2C.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/NotAckI2C.o.d"        -g -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/NotAckI2C.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/RestartI2C.o: RestartI2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/RestartI2C.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  RestartI2C.c  -o ${OBJECTDIR}/RestartI2C.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/RestartI2C.o.d"        -g -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/RestartI2C.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/StartI2C.o: StartI2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/StartI2C.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  StartI2C.c  -o ${OBJECTDIR}/StartI2C.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/StartI2C.o.d"        -g -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/StartI2C.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/StopI2C.o: StopI2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/StopI2C.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  StopI2C.c  -o ${OBJECTDIR}/StopI2C.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/StopI2C.o.d"        -g -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/StopI2C.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/WriteI2C.o: WriteI2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/WriteI2C.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  WriteI2C.c  -o ${OBJECTDIR}/WriteI2C.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/WriteI2C.o.d"        -g -omf=elf -O1 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/WriteI2C.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemblePreproc
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: archive
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/4013-I2Clib.X.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/4013-I2Clib.X.${OUTPUT_SUFFIX} 
	${MP_AR} $(MP_EXTRA_AR_PRE)  -omf=elf -r dist/${CND_CONF}/${IMAGE_TYPE}/4013-I2Clib.X.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/4013-I2Clib.X.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/4013-I2Clib.X.${OUTPUT_SUFFIX} 
	${MP_AR} $(MP_EXTRA_AR_PRE)  -omf=elf -r dist/${CND_CONF}/${IMAGE_TYPE}/4013-I2Clib.X.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
