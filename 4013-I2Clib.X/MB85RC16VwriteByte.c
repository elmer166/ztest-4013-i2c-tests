/*! \file MB85RC16VwriteByte.c
 *
 *  \brief  Write a byte to the MB85RC16V FRAM
 *
 *
 *  \date 25-Nov-12
 *  \author JJMcD
 *
 */

#include "i2c.h"
#include "MB85RC16V.h"

void blinky( int );

/*! Write a value to the MB85RC16V.  */
/*! Writes a byte to the MB85RC16V FRAM.
 *
 *  \param  ucDevice unsigned char Device I2C address
 *  \param  uAddress unsigned int  Address of memory cell to write
 *  \param  uValue   unsigned char Value to write to the FRAM
 *  \return none
 */
void MB85RC16VwriteByte( unsigned char ucDevice,
                         unsigned int  uAddress,
                         unsigned char ucValue )
{
    unsigned char ucControlByte;
    unsigned char ucLowAddr;

    /* Write condition is a zero bit so the control byte is formed
     * merely by shifting the address left one bit.  However, on the
     * MB85RC16V the control byte includes the high three bits of the
     * memory address as bits 1, 2, and 3, with bit 0 still 0 for write */
    ucControlByte = ( ucDevice<<1 & 0xf0 ) | ( (uAddress & 0x700)>>7 );
    ucLowAddr = uAddress & 0xff;
    
    StartI2C();         /* Start I2C transaction            */
    WriteI2C(ucControlByte); /* Address of MCP4726 | write  */
    WriteI2C(ucLowAddr);/* high 4 bits of value             */
    WriteI2C(ucValue);  /* Low 8 bits of value              */    
    StopI2C();          /* Stop the transaction             */
}
