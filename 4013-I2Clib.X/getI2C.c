/*! \file getI2C.c
 *
 *  \brief  Read a single byte from Bus
 *
 *  The function sets the master receive bit then waits for the
 *  receive buffer to fill.  It returns the contents of the
 *  receive buffer.
 *
 *  \date 21-Nov-12
 *  \author JJMcD
 *
 */

#include <xc.h>
#include "i2c.h"

/*! Read a single byte from Bus.  */
/*! Enables master receive then waits for the receive buffer to be full.
 *
 *  \param none
 *  \return Contents of I2C receive buffer
 *
 */
unsigned int getI2C(void)
{
    IdleI2C();
    I2CCONbits.RCEN = 1;            /* Enable Master receive            */
    Nop();
    while(!I2CSTATbits.RBF);        /* Wait for receive bufer to be full*/
    return(I2CRCV);                 /* Return data in buffer            */
}

