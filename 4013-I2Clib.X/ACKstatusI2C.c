/*! \file ACKstatusI2C.c
 *
 *  \brief  Return the Acknowledge status on the bus
 *
 *
 *  \date 22-Nov-12
 *  \author JJMcD
 *
 */

#include <xc.h>
#include "i2c.h"

/*! Return the Acknowledge status on the bus   */
/*! This function checks the state of the ACKSTAT bit in the I2C
 *  status register and returns TRUE if clear.
 *
 *  \param none
 *  \return Inverse of state of I2CSTATbits.ACKSTAT
 *
 */
unsigned int ACKstatusI2C( void )
{
    return (!I2CSTATbits.ACKSTAT);
}