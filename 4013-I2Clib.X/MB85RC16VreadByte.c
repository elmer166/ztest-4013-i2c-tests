/*! \file MB85RC16VreadByte.c
 *
 *  \brief  Read a byte from the FRAM
 *
 *
 *  \date 21-Nov-12
 *  \author JJMcD
 *
 */

#include "i2c.h"
#include <xc.h>
#include "MB85RC16V.h"

/*! Read a byte from the MB85RC16V. */
/*! This function reads a byte from the MB85RC16V FRAM.
 *
 *  \param ucDevice   unsigned char - MB85RC16V device address
 *  \param uAddress   unsigned int  - Address of location to read
 *  \return unsigned char contents of memory cell
 *
 */
unsigned char MB85RC16VreadByte( unsigned char ucDevice,
                                 unsigned int  uAddress )
{
    unsigned char ucControlByte;
    unsigned char ucLowAddr;
    unsigned char ucResult;

    /* Write condition is a zero bit so the control byte is formed
     * merely by shifting the address left one bit.  However, on the
     * MB85RC16V the control byte includes the high three bits of the
     * memory address as bits 1, 2, and 3, with bit 0 still 0 for write */
    ucControlByte = ( ucDevice<<1 & 0xf0 ) | ( (uAddress & 0x700)>>7 );
    ucLowAddr = uAddress & 0xff;

    StartI2C();                 /* Start I2C transaction            */
    WriteI2C( ucControlByte );  /* Send bus Address                 */
    WriteI2C(ucLowAddr);        /* Address of desired register      */
    RestartI2C();               /* Restart so can send read         */
    WriteI2C( ucControlByte+1 );/* Send bus address with write      */
    ucResult = getI2C();        /* Get answer from MB85RC16V        */
    NotAckI2C();                /* NAK result to stop answers       */
    StopI2C();                  /* Send stop on bus                 */

    return ( ucResult );
}

