/*! \file StopI2C.c
 *
 *  \brief  Generates a bus stop condition
 *
 *
 *  \date 21-Nov-12
 *  \author JJMcD
 *
 */

#include <xc.h>
#include "i2c.h"

/*! Generates a bus stop condition.  */
/*! This function generates an I2C stop condition and returns status
 *  of the Stop.
 *
 *  \param none
 *  \return I2CSTATbits.P - stop condition detected bit
 */
unsigned int StopI2C(void)
{
    IdleI2C();
    I2CCONbits.PEN = 1;             /* Generate stop condition      */
    while (I2CCONbits.PEN);         /* Wait for Stop                */
    return(I2CSTATbits.P);          /* return status                */
}

