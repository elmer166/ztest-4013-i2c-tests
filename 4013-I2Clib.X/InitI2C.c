/*! \file InitI2C.c
 *
 *  \brief  Initializes the I2C peripheral
 *
 *
 *  \date 21-Nov-12
 *  \author JJMcD
 *
 */

#include <xc.h>
#include "i2c.h"

/*! Initializes the I2C peripheral.   */
/*! This function sets the I2C baud rate, sets up master mode with
 *  no slew rate control, clears the transmit and receive
 *  registers and then enables the I2C peripheral.  The baud rate
 *  is calculated in i2c.h.
 *
 *  \param none
 *  \return none
 */
void InitI2C(void)
{
    /* Now we will initialise the I2C peripheral for Master Mode,
     *  No Slew Rate Control, and leave the peripheral switched off.
     *
     * bit name   val meaning
     *  15 I2CEN   0  Enable (disable for now)
     *  14 unused  0
     *  13 I2CSIDL 0  Stop in idle mode (no)
     *  12 SCLREL  0  Release control bit (no - slave only)
     *  11 IPMIEN  0  IPMI enable (no)
     *  10 A10M    0  10-bit slave address (no - slave only)
     *   9 DISSLW  1  Disable slew rate control (yes)
     *   8 SMEN    0  SMBbus input levels (no)
     *   7 GCEN    0  General call enable (no - slave only)
     *   6 STREN   0  Clock stretch bit (no - slave only)
     *   5 ACKDT   0  Acknowledge data bit (no)
     *   4 ACKEN   0  Acknowledge sequence enable (no)
     *   3 RCEN    0  Receive enable (no)
     *   2 PEN     0  Stop condition enable (no)
     *   1 RSEN    0  Repeated start condition enable (no)
     *   0 SEN     0  Start condition enable (no)
     */
    I2CCON = 0x0200;

    /* Set the I2C BRG Baud Rate.  The value for BRGVAL is
     * calculated in i2c.h */
    I2CBRG = BRGVAL;

    /* Clear the receive and transmit buffers */
    I2CRCV = 0x0000;
    I2CTRN = 0x0000;

    /* Now we can enable the peripheral */
    I2CCONbits.I2CEN = 1;
}
