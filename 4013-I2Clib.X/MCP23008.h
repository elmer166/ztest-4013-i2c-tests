/*! \file  MCP23008.h
 *
 *  \brief  Contains register definitions for MCP23008
 *
 *  This header contains constants defining the 11 internal registers
 *  in the Microchip MCP23008 8-bit I/O Expander.
 *
 *  In addition, function prototypes are provided.
 *
 *  \date 21-Nov-12
 *  \author JJMcD
 *
 */
#ifndef MCP23008_H
#define	MCP23008_H

#ifdef	__cplusplus
extern "C" {
#endif

/*! MCP23008  I/O direction register */
#define MCP23008_IODIR   0x00
/*! MCP23008 Input polarity register */
#define MCP23008_IOPOL   0x01
/*! MCP23008 Interrupt on change pins */
#define MCP23008_GPINTEN 0x02
/*! MCP23008 Default value register */
#define MCP23008_DEFVAL  0x03
/*! MCP23008 Interrupt-on-change control register */
#define MCP23008_INTCON  0x04
/*! MCP23008 I/O expander configuration register */
#define MCP23008_IOCON   0x05
/*! MCP23008 GPIO pull-up resistor register */
#define MCP23008_GPPU    0x06
/*! MCP23008 Interrupt flag register */
#define MCP23008_INTF    0x07
/*! MCP23008 Interrupt captured value for port register */
#define MCP23008_INTCAP  0x08
/*! MCP23008 General purpose I/O port register */
#define MCP23008_GPIO    0x09
/*! MCP23008 Output latch register */
#define MCP23008_OLAT    0x0a

/*! Write a register in the MCP23008. */
void MCP23008writeRegister( unsigned char, unsigned char, unsigned char );

/*! Read a register in the MCP23008. */
unsigned char MCP23008readRegister( unsigned char, unsigned char );

#ifdef	__cplusplus
}
#endif

#endif	/* MCP23008_H */

