/*! \file StartI2C.c
 *
 *  \brief  Generates an I2C Start Condition
 *
 *
 *  \date 21-Nov-12
 *  \author JJMcD
 *
 */

#include <xc.h>
#include "i2c.h"

/*! Generates an I2C Start Condition.   */
/*! This function generates an I2C start condition and returns status
 *  of the Start.
 *
 *  \param none
 *  \return I2CSTATbits.S - Start condition detected
 */
unsigned int StartI2C(void)
{
    IdleI2C();                      /* Wait for bus idle            */
    I2CCONbits.SEN = 1;             /* Generate Start Condition     */
    while (I2CCONbits.SEN);         /* Wait for Start Condition     */
    return(I2CSTATbits.S);          /* Optionally return status     */
}

