/*! \file MCP23008readRegister.c
 *
 *  \brief  Read the MCP23008 register contents
 *
 *
 *  \date 21-Nov-12
 *  \author JJMcD
 *
 */

#include "i2c.h"
#include <xc.h>

/*! Write a register in the MCP23008. */
/*! This function sets a value in a MCP23008 register. The control
 *  byte to be sent on the I2C bus is first calculated by shifting
 *  the device address left one bit.  Then an I2C transaction is initiated
 *  and when complete, the control byte, register address, and desired
 *  register contents are sent on the I2C bus.  Finally a transaction
 *  stop sequence is initiated.
 *
 *  \param ucDevice   unsigned char - MCP23008 device address
 *  \param ucRegister unsigned char - Address of register to read
 *  \return unsigned char contents of register
 *
 */
unsigned char MCP23008readRegister( unsigned char ucDevice,
                                    unsigned char ucRegister )
{
    unsigned char ucControlByte;
    unsigned char ucResult;

    /* Write condition is a zero bit so the control byte is formed
     * merely by shifting the address left one bit */
    ucControlByte = ucDevice<<1;

    StartI2C();                 /* Start I2C transaction            */
    WriteI2C( ucControlByte );  /* Send bus Address                 */
    WriteI2C(ucRegister);       /* Address of desired register      */
    RestartI2C();               /* Restart so can send read         */
    WriteI2C( ucControlByte+1 );/* Send bus address with write      */
    ucResult = getI2C();        /* Get answer from MCP23008         */
    NotAckI2C();                /* NAK result to stop answers       */
    StopI2C();                  /* Send stop on bus                 */
    return ( ucResult );
}

