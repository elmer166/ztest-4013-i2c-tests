/*! \file  MB85RC16V.h
 *
 *  \brief  Contains register definitions for MB85RC16V
 *
 *  This header contains function prototypes for the Fujitsu
 *  MB85RC16V Ferroelectric Random Access Memory
 *
 *  \date 25-Nov-12
 *  \author JJMcD
 *
 */
#ifndef MB85RC16V_H
#define	MB85RC16V_H

#ifdef	__cplusplus
extern "C" {
#endif

/*! Write a value to the MB85RC16V. */
void MB85RC16VwriteByte( unsigned char, unsigned int, unsigned char );

/*! Read a byte from the MB85RC16V. */
unsigned char MB85RC16VreadByte( unsigned char, unsigned int );



#ifdef	__cplusplus
}
#endif

#endif	/* MB85RC16V_H */

