/*! \file NotAckI2C.c
 *
 *  \brief  Send a NAK on the bus
 *
 *
 *  \date 23-Nov-12
 *  \author JJMcD
 *
 */

#include <xc.h>
#include "i2c.h"

/*! Generates a NO Acknowledge on the Bus   */
/*! Sets the NO-ACK bit, then ACK enable and then waits for
 *  the NAK to complete.
 *
 *  \param none
 *  \return none
 */
void NotAckI2C(void)
{
    IdleI2C();
    I2CCONbits.ACKDT = 1;                   //Set for NotACk
    I2CCONbits.ACKEN = 1;
    while(I2CCONbits.ACKEN);                //wait for ACK to complete
    I2CCONbits.ACKDT = 0;                   //Set for NotACk
}


