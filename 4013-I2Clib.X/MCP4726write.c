/*! \file MCP4726write.c
 *
 *  \brief  Write a value to the MCP4726
 *
 *
 *  \date 24-Nov-12
 *  \author JJMcD
 *
 */

#include "i2c.h"

/*! Write a value to the MCP4726 DAC  */
/*! Writes a value to the default register of the DAC
 *
 *  \param  ucDevice unsigned char Device I2C address
 *  \param  uValue   unsigned int 12-bit value to write to the 4726
 *  \return none
 */
void MCP4726write( unsigned char ucDevice,
                   unsigned int uValue )
{
    unsigned char ucControlByte;
    unsigned char ucByteH;
    unsigned char ucByteL;

    /* Write condition is a zero bit so the control byte is formed
     * merely by shifting the address left one bit */
    ucControlByte = ucDevice<<1;
    /* Break value into two bytes */
    ucByteH = uValue >> 8;
    ucByteL = uValue & 0xff;

    StartI2C();         /* Start I2C transaction            */
    WriteI2C(ucControlByte); /* Address of MCP4726 | write  */
    WriteI2C(ucByteH);  /* high 4 bits of value             */
    WriteI2C(ucByteL);  /* Low 8 bits of value              */
    StopI2C();          /* Stop the transaction             */
}
