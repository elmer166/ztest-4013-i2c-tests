/*! \file RestartI2C.c
 *
 *  \brief  Generates a restart condition and returns the "Start heard last" bit.
 *
 *
 *  \date 21-Nov-12
 *  \author JJMcD
 *
 */

#include <xc.h>
#include "i2c.h"

/*! Generates a restart condition and returns the "Start heard last" bit.   */
/*! Function sets the restart bit and waits for the restart to happen. It
 *  then returns I2CSTATbits.S, the "Start heard last" bit.
 *
 *  \param none
 *  \return I2CSTATbits.S - start condition detected
 */
unsigned int RestartI2C(void)
{
    IdleI2C();
    //1 IFS0bits.MI2CIF = 0;
    I2CCONbits.RSEN = 1;            /* Generate restart             */
    while (I2CCONbits.RSEN);        /* Wait for restart             */
    //while ( !IFS0bits.MI2CIF);
    return(I2CSTATbits.S);          /* return status                */
}
