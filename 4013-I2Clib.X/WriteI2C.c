/*! \file WriteI2C.c
 *
 *  \brief  Writes a byte out to the bus
 *
 *
 *  \date 21-Nov-12
 *  \author JJMcD
 *
 */

#include <xc.h>
#include "i2c.h"

/*! Writes a byte out to the bus   */
/*! This function transmits the byte passed to the function
 *
 *  \param byte (unsigned char) byte to be written
 *  \return none
 *
 */
void WriteI2C(unsigned char byte)
{
    IdleI2C();                      /* Wait for bus to be idle          */
    I2CTRN = byte;                  /* Load byte to I2C Transmit buffer */
    while (I2CSTATbits.TBF);        /* wait for data transmission       */
}

