/*! \file  MCP4726.h
 *
 *  \brief Definitions for MCP4726 DAC
 *
 *  \author jjmcd
 *  \date January 9, 2013, 9:02 PM
 */

#ifndef MCP4726_H
#define	MCP4726_H

#ifdef	__cplusplus
extern "C" {
#endif

/*! Write a value to the MCP4726 DAC  */
void MCP4726write( unsigned char, unsigned int );



#ifdef	__cplusplus
}
#endif

#endif	/* MCP4726_H */

