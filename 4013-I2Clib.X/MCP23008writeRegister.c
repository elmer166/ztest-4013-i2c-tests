/*! \file MCP23008writeRegister.c
 *
 *  \brief  Write a register in the MCP23008
 *
 *
 *  \date 21-Nov-12
 *  \author JJMcD
 *
 */

#include "i2c.h"

/*! Write a register in the MCP23008. */
/*! This function sets a value in a MCP23008 register. The control
 *  byte to be sent on the I2C bus is first calculated by shifting
 *  the device address left one bit.  Then an I2C transaction is initiated
 *  and when complete, the control byte, register address, and desired
 *  register contents are sent on the I2C bus.  Finally a transaction
 *  stop sequence is initiated.
 *
 *  \param ucDevice   unsigned char - MCP23008 device address
 *  \param ucRegister unsigned char - Address of register to write
 *  \param ucValue    unsigned char - Desired contents of register
 *  \return none
 *
 */
void MCP23008writeRegister( unsigned char ucDevice,
                            unsigned char ucRegister,
                            unsigned char ucValue )
{
    unsigned char ucControlByte;

    /* Write condition is a zero bit so the control byte is formed
     * merely by shifting the address left one bit */
    ucControlByte = ucDevice<<1;

    StartI2C();         /* Start I2C transaction            */
    WriteI2C(ucControlByte); /* Address of MCP23008 | write */
    WriteI2C(ucRegister); /* Address of register to write   */
    WriteI2C(ucValue);  /* Desired register contents        */
    StopI2C();          /* Stop the transaction             */
}

